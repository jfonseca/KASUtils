# KASUtilis

Helper scripts for mouse events, color modifiers, and object detection

## Usage

### DESCRIPTION:

### Medthods Availible

* captureMouse(element) : Keeps track of the current mouse position, relative to an element.
* captureTouch(element) : Keeps track of the current (first) touch position, relative to an element.
* parseColor(color, toNumber) : Returns a color in the format: '#RRGGBB', or as a hex number if specified.
* rgbToHex(rgb) : Input RGB and returns a hex from it
* colorToRGB(color, alpha) : Hex to RGB
* colorToRGBObj(color, alpha) : Converts a color to the RGB string format: 'rgb(r,g,b)' or 'rgba(r,g,b,a)* colorRandom() : Picks random color and returns it as hex
* containsPoint(rect, x, y) : Determine if a rectangle contains the coordinates (x,y) within it's boundaries.
* intersects(rectA, rectB) : Determine if two rectangles overlap.
* randRange() : Allow a random call between 2 numbers


## Authors

* **Jose Fonseca** - _[Klip Art Studio](https://www.klipartstudio.com)_ - [GIT](https://gitlab.com/jfonseca/KASUtils.git)

## License

MIT
